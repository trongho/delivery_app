﻿(function ($) {
    function Index() {
        var $this = this;
        function initialize() {

            $(".popup").on('click', function (e) {
                modelPopup(this);
            });

            function modelPopup(reff) {
                var url = $(reff).data('url');
                var id = $(reff).data('id');
                $.get(url + "/" + id).done(function (data) {
                    debugger;
                    $('#modal-start-order').find(".modal-dialog").html(data);
                    $('#modal-start-order > .modal', data).modal("show");
                });

            }
        }

        $this.init = function () {
            initialize();
        };
    }
    $(function () {
        var self = new Index();
        self.init();
    });

    function loadmap($param) {
        goongjs.accessToken = '9IOlUggYUhs9DM8hgiw1Z4LhoGu8YRkLUc54xVxD';
        var map = new goongjs.Map({
            container: $param,
            style: 'https://tiles.goong.io/assets/goong_map_web.json',
            center: [105.80028, 21.0188],
            zoom: 13
        });
    }

    loadmap('myMap');

}(jQuery)); 