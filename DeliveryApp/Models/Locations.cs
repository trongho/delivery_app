﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryApp.Models
{
    public class Locations
    {
        public String Longitude { get; set; }
        public String Latitude { get; set; }

        public Locations(string longitude, string latitude)
        {
            Longitude = longitude;
            Latitude = latitude;
        }
    }
}
