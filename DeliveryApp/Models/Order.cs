﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryApp.Models
{
    public class Order
    {
        public String OrderID { get; set; }
        public String ShippingType { get; set; }
        public String Employee1 { get; set; }
        public String Employee2 { get; set; }
        public DateTime? RequiredDate { get; set; }
        public DateTime? ShippedDate { get; set; }
        public Decimal OrderFee { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? StopTime { get; set; }
        public Decimal OrderValue { get; set; }
        public Decimal Mass { get; set; }
        public Decimal Kilometers { get; set; }
        public String Result { get; set; }
        public String Reason { get; set; }
        public String ShippingMethod { get; set; }
        public String BussinesStaffID { get; set; }
        public String StartPoint { get; set; }
        public String EndPoint { get; set; }
        public String WarehouseID { get; set; }
        public String Note { get; set; }
        public String Status { get; set; }
        public String CustomerName { get; set; }
        public String CustomerAdress { get; set; }
        public String CustomerPhone { get; set; }
        public String CustomerContact { get; set; }
    }
}
