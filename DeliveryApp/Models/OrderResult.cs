﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryApp.Models
{
    public class OrderResult
    {
        public String ResultID { get; set; }
        public String ResultName { get; set; }
    }
}
