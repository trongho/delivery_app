﻿using DeliveryApp.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DeliveryApp.Controllers
{
    public class UserOrderController : Controller
    {
        public async Task<IActionResult> Index()
        {
            string URI = "http://localhost:5000/api/order/";
            List<Order> entrys = new List<Order>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<Order[]>(fileJsonString).ToList();
                        }
                        else
                        {
                            ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                        }

                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            entrys = entrys.Where(x => x.Employee1.Contains(DeliveryApp.Helpers.Common.loginResponse.Username)).ToList();
            return View(entrys);
        }

        [HttpGet("UserOrder/Edit/{OrderID}")]
        public async Task<IActionResult> Edit(string OrderID)
        {
            string URI = "http://localhost:5000/api/order/GetByID/" + OrderID;
            List<Order> entrys = new List<Order>();
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<Order[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return View(entrys[0]);
        }

        public async Task<IActionResult> Edit(Order order, String OrderID)
        {
            string URI = "http://localhost:5000/api/order/Put/" + OrderID;
            var content = new StringContent(JsonConvert.SerializeObject(order), Encoding.UTF8, "application/json");
            using (var client = new HttpClient())
            {
                using (var response = await client.PutAsync(URI, content))
                {

                    if (response.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return View(order);
                    }
                }
            }
            return View();
        }

        [HttpGet("UserOrder/Edit/StartOrder/{OrderID}")]
        public async Task<IActionResult> StartOrder(string OrderID)
        {
            string URI = "http://localhost:5000/api/order/GetByID/" + OrderID;
            List<Order> entrys = new List<Order>();
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<Order[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return PartialView("StartOrder", entrys[0]);
        }

        public async Task<IActionResult> StartOrderAction(Order order, String OrderID)
        {
            string URI = "http://localhost:5000/api/order/StartOrder/" + OrderID;
            var content = new StringContent(JsonConvert.SerializeObject(order), Encoding.UTF8, "application/json");
            using (var client = new HttpClient())
            {
                using (var response = await client.PutAsync(URI, content))
                {

                    if (response.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return PartialView("CancelOrder", order);
                    }
                }
            }
            return View();
        }

        [HttpGet("UserOrder/Edit/{OrderID}/GetKilometers")]
        public ActionResult GetKilometers(string OrderID, String StartPoint, String StopPoint)
        {
            String firstString = "https://rsapi.goong.io/DistanceMatrix?origins=";
            String threeString = "&destinations=";
            String fiveString = "&vehicle=";
            String sevenString = "&api_key=";
            String APiKey = "rDCEpOhGmuPf78dMFbiLh7jNEzeHaJEAj2qVM9ns";
            var uri = firstString + "20.981971,105.864323" + threeString + "21.031011,105.783206" + fiveString + "bike" + sevenString + APiKey;

            var json = "";
            using (var client = new WebClient())
            {
                json = client.DownloadString(uri);
            }

            dynamic data = JsonConvert.DeserializeObject(json);
            Decimal kilometers = data.rows[0].elements[0].distance.value;
            ViewBag.Kilometers = kilometers.ToString();

            return Json(kilometers);
        }

        public async Task<IActionResult> Ended(Order order,String OrderID)
        {
            string URI = "http://localhost:5000/api/order/Ended/" + OrderID;
            var content = new StringContent(JsonConvert.SerializeObject(order), Encoding.UTF8, "application/json");
            using (var client = new HttpClient())
            {
                using (var response = await client.PutAsync(URI, content))
                {

                    if (response.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Index");
                    }
                }
            }
            return RedirectToAction("Index");
        }

        [HttpGet("UserOrder/Edit/CancelOrder/{OrderID}")]
        public async Task<IActionResult> CancelOrder(string OrderID)
        {
            string URI = "http://localhost:5000/api/order/GetByID/" + OrderID;
            List<Order> entrys = new List<Order>();
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<Order[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return PartialView("CancelOrder", entrys[0]);
        }

        public async Task<IActionResult> CancelOrderAction(Order order, String OrderID)
        {
            string URI = "http://localhost:5000/api/order/Put/" +OrderID;
            var content = new StringContent(JsonConvert.SerializeObject(order), Encoding.UTF8, "application/json");
            using (var client = new HttpClient())
            {
                using (var response = await client.PutAsync(URI, content))
                {

                    if (response.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return PartialView("CancelOrder", order);
                    }
                }
            }
            return View();
        }

        [HttpGet("UserOrder/Map/{OrderID}")]
        public async Task<ActionResult> Map(String OrderID)
        {
            string URI = "http://localhost:5000/api/order/GetByID/" + OrderID;
            List<Order> entrys = new List<Order>();
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<Order[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            ViewBag.OrderID =OrderID;
            return View(entrys[0]);
        }

        [HttpGet("UserOrder/Map/{OrderID}/GetLocations")]
        public ActionResult GetLocations()
        {
            var locations = new List<Models.Locations>() {
                new Models.Locations ("105.78903","21.02524"),
                new Models.Locations("105.79553","21.03027"),
                new Models.Locations("105.79621","21.00963")
            };

            return Json(locations);
        }


        [HttpGet("UserOrder/Map/{OrderID}/TellMeDate")]
        public string TellMeDate()
        {
            return DateTime.Today.ToString();
        }
    }
}
