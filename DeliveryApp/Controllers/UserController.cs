﻿using DeliveryApp.Helpers;
using DeliveryApp.Models;
using DeliveryApp.Requests;
using DeliveryApp.Responses;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Threading.Tasks;

namespace DeliveryApp.Controllers
{
    public class UserController : Controller
    {
        public async Task<IActionResult> IndexAsync(string username,string fullname)
        {
            string URI = "http://localhost:5000/api/user";
            List<User> entrys = new List<User>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<User[]>(fileJsonString).ToList();
                        }
                        else
                        {
                            ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                        }

                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            if (!string.IsNullOrEmpty(username))
            {
                entrys = entrys.Where(x => x.UserName.Contains(username)).ToList();
            }
            if (!string.IsNullOrEmpty(fullname))
            {
                entrys = entrys.Where(x => x.FullName.Contains(fullname)).ToList();
            }
            if (DeliveryApp.Helpers.Common.loginResponse.Role.Equals("User"))
            {
                return View("AccessDenied");
            }
            return View(entrys);
        }


        public ViewResult Details() => View();


        [HttpGet("User/Details/{UserID}")]
        public async Task<IActionResult> Details(string UserID)
        {
            string URI = "http://localhost:5000/api/user/GetByID/" + UserID;
            List<User> entrys = new List<User>();
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<User[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            if (DeliveryApp.Helpers.Common.loginResponse.Role.Equals("User")&& !DeliveryApp.Helpers.Common.loginResponse.UserID.Equals(UserID))
            {
                return View("AccessDenied");
            }
            return View(entrys[0]);
        }

        public async Task<IActionResult> Login(LoginRequest loginRequest)
        {
            string URI = "http://localhost:5000/api/user/login";
            LoginResponse loginResponse = new LoginResponse();


            if (Request.Cookies["Username"] != null)
                loginRequest.Username = Request.Cookies["Username"];
            if (Request.Cookies["Password"] != null)
                loginRequest.Password = Request.Cookies["Password"];

            using (var client = new HttpClient())
            {
                var content = new StringContent(JsonConvert.SerializeObject(loginRequest), Encoding.UTF8, "application/json");
                try
                {
                    using (var response = await client.PostAsync(URI, content))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            loginResponse = JsonConvert.DeserializeObject<LoginResponse>(fileJsonString);
                            Common.loginResponse = loginResponse;

                            CookieOptions option = new CookieOptions();
                            option.Expires = DateTime.Now.AddSeconds(10);
                            Response.Cookies.Append("Username", loginRequest.Username, option);
                            Response.Cookies.Append("Password", loginRequest.Password, option);

                            return View("../Home/Index");

                           
                        }
                        else
                        {
                            ModelState.AddModelError(string.Empty, "Username or Password is Incorrect");
                        }
                    }
                }
                catch (HttpRequestException e) { Console.WriteLine(e.InnerException.Message); }
            }
            return View(loginRequest);
        }

        public async Task<IActionResult> Create(User user)
        {
            string URI = "http://localhost:5000/api/user/Post";
            using (var client = new HttpClient())
            {
                var content = new StringContent(JsonConvert.SerializeObject(user), Encoding.UTF8, "application/json");
                try
                {
                    using (var response = await client.PostAsync(URI, content))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            return RedirectToAction("Index");
                        }
                        else
                        {
                            ModelState.AddModelError(string.Empty, "Nhập thiếu");
                        }
                    }
                }
                catch (HttpRequestException e)
                {

                }
            }
            return View();
        }

        [HttpGet("User/Edit/{UserID}")]
        public async Task<IActionResult> Edit(string UserID)
        {
            string URI = "http://localhost:5000/api/user/GetByID/" + UserID;
            List<User> entrys = new List<User>();
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<User[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return PartialView("Edit", entrys[0]);
        }

        public async Task<IActionResult> Edit(User user, String UserID)
        {
            string URI = "http://localhost:5000/api/user/Put/" + UserID;
            var content = new StringContent(JsonConvert.SerializeObject(user), Encoding.UTF8, "application/json");
            using (var client = new HttpClient())
            {
                using (var response = await client.PutAsync(URI, content))
                {

                    if (response.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return PartialView("Edit", user);
                    }
                }
            }
            return View();
        }

        [HttpGet("User/ChangePassword/{UserID}")]
        public async Task<IActionResult> ChangePassword()
        {
            ChangePassword changePassword = new ChangePassword();
            changePassword.UserID = DeliveryApp.Helpers.Common.loginResponse.UserID;
            return View(changePassword);
        }

        public async Task<IActionResult> ChangePassword(ChangePassword changePassword, String UserID)
        {
            string URI = "http://localhost:5000/api/user/ChangePassword/" + UserID;
            var content = new StringContent(JsonConvert.SerializeObject(changePassword), Encoding.UTF8, "application/json");
            if (ModelState.IsValid)
            {
                using (var client = new HttpClient())
                {
                    using (var response = await client.PutAsync(URI, content))
                    {
                        if (!DeliveryApp.Helpers.Common.loginResponse.Password.Equals(changePassword.oldPassword))
                        {
                            ModelState.AddModelError(string.Empty, "Mật khẩu cũ không đúng");
                            return View("ChangePassword", changePassword);
                        }
                        if (response.IsSuccessStatusCode)
                        {
                            return RedirectToAction("Index");
                        }
                        else
                        {
                            return View("ChangePassword", changePassword);
                        }
                    }
                }
            }
            return View();
        }

        [HttpGet("User/Delete/{UserID}")]
        public async Task<IActionResult> Delete(string UserID)
        {
            string URI = "http://localhost:5000/api/user/GetByID/" + UserID;
            List<User> entrys = new List<User>();
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<User[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return View(entrys[0]);
        }


        public async Task<IActionResult> DeleteAction(String UserID)
        {
            string URI = "http://localhost:5000/api/user/Delete/" + UserID;
            using (var client = new HttpClient())
            {
                using (var response = await client.DeleteAsync(URI))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Index");
                    }
                }
            }
            return View();
        }

        public async Task<IActionResult> Logout(User user)
        {
            Common.loginResponse = null;
            Response.Cookies.Delete("Username");
            Response.Cookies.Delete("Password");
            return RedirectToAction("Login");
        }

    }
}
