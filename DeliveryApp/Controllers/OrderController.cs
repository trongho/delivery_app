﻿using DeliveryApp.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.FileProviders;
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DeliveryApp.Controllers
{
    public class OrderController : Controller
    {
        private readonly IHostingEnvironment _hostingEnvironment;

        public OrderController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        public async Task<IActionResult> Index()
        {
            string URI = "http://localhost:5000/api/order";
            List<Order> entrys = new List<Order>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<Order[]>(fileJsonString).ToList();
                        }
                        else
                        {
                            ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                        }

                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            if (DeliveryApp.Helpers.Common.loginResponse.Role.Equals("User"))
            {
                return View("AccessDenied");
            }
            return View(entrys);
        }

        public async Task<List<OrderResult>> OrderResult()
        {
            string URI = "http://localhost:5000/api/orderresult";
            List<OrderResult> entrys = new List<OrderResult>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<OrderResult[]>(fileJsonString).ToList();
                        }
                        else
                        {
                            ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                        }

                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        //public async Task<IActionResult> Import()
        //{
        //    string URI = "http://localhost:5000/api/order/Import";
        //    List<Order> entrys = new List<Order>();
        //    using (var client = new HttpClient())
        //    {
        //        try
        //        {
        //            using (var response = await client.GetAsync(URI))
        //            {
        //                if (response.IsSuccessStatusCode)
        //                {
        //                    var fileJsonString = await response.Content.ReadAsStringAsync();
        //                    entrys = JsonConvert.DeserializeObject<Order[]>(fileJsonString).ToList();
        //                }
        //                else
        //                {
        //                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
        //                }

        //            }
        //        }
        //        catch (HttpRequestException e)
        //        {
        //            Console.WriteLine(e);
        //        }
        //    }
        //    return RedirectToAction("Index");
        //}

        //public async Task<IActionResult> Export()
        //{
        //    string URI = "http://localhost:5000/api/order/Export";
        //    using (var client = new HttpClient())
        //    {
        //        try
        //        {
        //            using (var response = await client.GetAsync(URI))
        //            {
        //                if (response.IsSuccessStatusCode)
        //                {
        //                    var fileJsonString = await response.Content.ReadAsStringAsync();
        //                }
        //                else
        //                {
        //                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
        //                }

        //            }
        //        }
        //        catch (HttpRequestException e)
        //        {
        //            Console.WriteLine(e);
        //        }
        //    }

        //    return RedirectToAction("Index");
        //}

        //[HttpPost]
        //public async Task<IActionResult> UploadFile(IFormFile file)
        //{
        //    string apiResponse = "";
        //    using (var httpClient = new HttpClient())
        //    {
        //        var form = new MultipartFormDataContent();
        //        using (var fileStream = file.OpenReadStream())
        //        {
        //            form.Add(new StreamContent(fileStream), "file", file.FileName);
        //            using (var response = await httpClient.PostAsync("http://localhost:5000/api/order/UploadFile", form))
        //            {
        //                if (response.IsSuccessStatusCode)
        //                {
        //                    apiResponse = await response.Content.ReadAsStringAsync();
        //                    await Import();
        //                    await Export();
        //                    await DowloadFileAsync();
        //                }
        //            }
        //        }
        //    }
        //    return RedirectToAction("Index");
        //}

        [HttpPost]
        public async Task<IActionResult> UploadFile(IFormFile file)
        {

            var form = new MultipartFormDataContent();
            using (var fileStream = file.OpenReadStream())
            {
                string path = Path.Combine(_hostingEnvironment.WebRootPath, "ExcelFile/" + file.FileName);
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    await file.CopyToAsync(stream);

                }
                form.Add(new StreamContent(fileStream), "file", file.FileName);


                List<Order> orders = await Import(file.FileName);
                foreach (Order order in orders)
                {
                    if (await Create(order) == true)
                    {
                        await Export(orders, file.FileName);
                    }
                }
            }

            return RedirectToAction("Index");
        }

        public String GetLngLat(String adress)
        {

            var fileJsonString = "";
            String firstString = "https://rsapi.goong.io/geocode?address=";
            String threeString = "&api_key=";
            String APiKey = "rDCEpOhGmuPf78dMFbiLh7jNEzeHaJEAj2qVM9ns";
            var uri = firstString + adress + threeString + APiKey;
            var json = "";
            String lnglat = "";
            using (var client = new WebClient())
            {
                json = client.DownloadString(uri);
            }

            dynamic data = JsonConvert.DeserializeObject(json);
            String lng = data.results[0].geometry.location.lng;
            String lat = data.results[0].geometry.location.lat;
            lnglat = lng + "," + lat;
            return lnglat;
        }

        public async Task<int> getOrderIDAsync()
        {
            string URI = "http://localhost:5000/api/order/lastID";
            String lastID = "";
            int orderID = 1;
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            lastID = fileJsonString;
                            if (lastID != null)
                            {
                                orderID = int.Parse(lastID) + 1;
                            }
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return orderID;
        }

        public async Task<List<Order>> Import(String fileName)
        {
            string rootFolder = _hostingEnvironment.WebRootPath;
            FileInfo file = new FileInfo(Path.Combine(rootFolder, "ExcelFile/" + fileName));
            List<Order> orders = new List<Order>();


            using (ExcelPackage package = new ExcelPackage(file))
            {
                ExcelWorksheet workSheet = package.Workbook.Worksheets.FirstOrDefault();
                int totalRows = workSheet.Dimension.Rows;
                int orderId = await getOrderIDAsync();
                for (int i = 5; i <= totalRows; i++)
                {
                    String selected = workSheet.Cells[i, 1].Text.Trim();
                    if (selected.Equals("x"))
                    {
                        orders.Add(new Order
                        {
                            OrderID = $"{orderId:D10}",
                            RequiredDate = DateTime.Parse("01-01-2021"),
                            WarehouseID = workSheet.Cells[i, 6].Text.Trim(),
                            ShippingMethod = workSheet.Cells[i, 11].Text.Trim(),
                            OrderValue = Decimal.Parse(workSheet.Cells[i, 12].Text.Trim()),
                            BussinesStaffID = workSheet.Cells[i, 13].Text.Trim(),
                            Note = workSheet.Cells[i, 14].Text.Trim(),
                            ShippingType = "",
                            ShippedDate = null,
                            Employee1 = "",
                            Employee2 = "",
                            OrderFee = 0,
                            StartTime = null,
                            StopTime = null,
                            Mass = 0,
                            Kilometers = 0,
                            Result = "",
                            Reason = "",
                            CustomerAdress = workSheet.Cells[i, 8].Text.Trim(),
                            CustomerName = workSheet.Cells[i, 7].Text.Trim(),
                            CustomerPhone = workSheet.Cells[i, 10].Text.Trim(),
                            CustomerContact = workSheet.Cells[i, 9].Text.Trim(),
                            StartPoint = "",
                            EndPoint = GetLngLat(workSheet.Cells[i, 8].Text.Trim()),
                            Status = "New",

                        });
                        orderId++;
                    }
                }
            }
            return orders;
        }

        public async Task<bool> Export(List<Order> orders, String fileName)
        {
            string rootFolder = _hostingEnvironment.WebRootPath;
            FileInfo file = new FileInfo(Path.Combine(rootFolder, "ExcelFile/" + fileName));

            using (ExcelPackage package = new ExcelPackage(file))
            {

                ExcelWorksheet worksheet = package.Workbook.Worksheets.FirstOrDefault();
                int totalRows = orders.Count;

                int i = 0;
                for (int row = 5; row <= totalRows + 4; row++)
                {
                    worksheet.Cells[row, 2].Value = orders[i].OrderID;
                    i++;
                }
                package.Save();

            }
            return true;
        }

        public async Task<bool> Create(Order order)
        {
            bool flag = false;
            string URI = "http://localhost:5000/api/order/Post";
            using (var client = new HttpClient())
            {
                var content = new StringContent(JsonConvert.SerializeObject(order), Encoding.UTF8, "application/json");
                try
                {
                    using (var response = await client.PostAsync(URI, content))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            flag = true;
                        }
                        else
                        {
                            flag = false;
                        }
                    }
                }
                catch (HttpRequestException e)
                {

                }
            }
            return flag;
        }

        public async Task<FileResult> DowloadFileAsync(string fileName)
        {
            string filePath = System.IO.Path.Combine(_hostingEnvironment.WebRootPath);
            IFileProvider provider = new PhysicalFileProvider(filePath);
            IFileInfo fileInfo = provider.GetFileInfo(fileName);
            var readStream = fileInfo.CreateReadStream();
            var mimeType = "application/vnd.openxmlformatsofficedocument.spreadsheetml.sheet";
            return File(readStream, mimeType, fileName);
        }

        private string GetContentType(string path)
        {
            var types = GetMimeTypes();
            var ext = Path.GetExtension(path).ToLowerInvariant();
            return types[ext];
        }

        private Dictionary<string, string> GetMimeTypes()
        {
            return new Dictionary<string, string>
            {
                {".txt", "text/plain"},
                {".pdf", "application/pdf"},
                {".doc", "application/vnd.ms-word"},
                {".docx", "application/vnd.ms-word"},
                {".xls", "application/vnd.ms-excel"},
                {".xlsx", "application/vnd.openxmlformatsofficedocument.spreadsheetml.sheet"},  
                {".png", "image/png"},
                {".jpg", "image/jpeg"},
                {".jpeg", "image/jpeg"},
                {".gif", "image/gif"},
                {".csv", "text/csv"}
            };
        }

        //public async Task<IActionResult> DowloadFileAsync()
        //{
        //    WebClient webClient = new WebClient();
        //    webClient.DownloadFileAsync(new Uri("http://192.168.1.182:2021/wwwroot/ExcelFile/importOrder.xlsx"), @"c:\importOrder.xlsx");
        //    return RedirectToAction("Index");
        //}

        [HttpDelete]
        public async Task<IActionResult> DeleteFile()
        {
            string fileName = @"importOrder.xlsx";
            var path = Path.Combine(
                        Directory.GetCurrentDirectory(), "wwwroot/ExcelFile",
                        fileName);
            FileInfo file = new FileInfo(path);
            if (file.Exists)//check file exsit or not  
            {
                file.Delete();
            }
            else
            {
            }
            return RedirectToAction("Index");
        }

        [HttpGet("Order/Create")]
        public ViewResult Create() => View();


        //public async Task<IActionResult> Create(Order order)
        //{
        //    string URI = "http://localhost:5000/api/order/Post";
        //    using (var client = new HttpClient())
        //    {
        //        var content = new StringContent(JsonConvert.SerializeObject(order), Encoding.UTF8, "application/json");
        //        try
        //        {
        //            using (var response = await client.PostAsync(URI, content))
        //            {
        //                if (response.IsSuccessStatusCode)
        //                {
        //                    return RedirectToAction("Index");
        //                }
        //                else
        //                {
        //                    ModelState.AddModelError(string.Empty, "Nhập thiếu");
        //                }
        //            }
        //        }
        //        catch (HttpRequestException e)
        //        {

        //        }
        //    }
        //    return View();
        //}

        public ViewResult Details() => View();


        [HttpGet("Order/Details/{OrderID}")]
        public async Task<IActionResult> Details(string OrderID)
        {
            string URI = "http://localhost:5000/api/order/GetByID/" + OrderID;
            List<Order> entrys = new List<Order>();
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<Order[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return View(entrys[0]);
        }

        [HttpGet("Order/Edit/{OrderID}")]
        public async Task<IActionResult> Edit(string OrderID, OrderResult orderResult)
        {
            string URI = "http://localhost:5000/api/order/GetByID/" + OrderID;
            List<Order> entrys = new List<Order>();
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<Order[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            List<OrderResult> orderResults = new List<OrderResult>();
            orderResults = await OrderResult();
            orderResults.Insert(0, new OrderResult { ResultID = "0", ResultName = "Choose" });
            ViewBag.ListOfOrderResult = orderResults;

            String selectedOrderResult = orderResult.ResultID;
            ViewBag.selectedOrderResult = selectedOrderResult;
            return View(entrys[0]);
        }

        public async Task<IActionResult> Edit(Order order, String OrderID)
        {
            string URI = "http://localhost:5000/api/order/Put/" + OrderID;
            var content = new StringContent(JsonConvert.SerializeObject(order), Encoding.UTF8, "application/json");
            using (var client = new HttpClient())
            {
                using (var response = await client.PutAsync(URI, content))
                {

                    if (response.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return View(order);
                    }
                }
            }
            return View();
        }


        public async Task<IActionResult> Delete(string OrderID)
        {
            string URI = "http://localhost:5000/api/order/GetByID/" + OrderID;
            List<Order> entrys = new List<Order>();
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<Order[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return View(entrys[0]);
        }

        [HttpGet("Order/Delete/{OrderID}")]
        public async Task<IActionResult> DeleteAction(String OrderID)
        {
            string URI = "http://localhost:5000/api/order/Delete/" + OrderID;
            using (var client = new HttpClient())
            {
                using (var response = await client.DeleteAsync(URI))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Index");
                    }
                }
            }
            return RedirectToAction("Index");
        }
    }
}
