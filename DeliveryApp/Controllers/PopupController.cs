﻿using DeliveryApp.Models;
using DevExpress.AspNetCore.Spreadsheet;
using DevExpress.XtraSpreadsheet.Export;
using DevExpress.Spreadsheet;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using System.Text;

namespace DeliveryApp.Controllers
{
    public class PopupController : Controller
    {
        const string DocumentName = "ProfitAndLoss.xlsx";
        const string XlsxContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        private readonly IHostingEnvironment env;

        public async Task<IActionResult> IndexAsync()
        {
            String OrderID = "0000000001";
            string URI = "http://localhost:5000/api/order/GetByID/" + OrderID;
            List<Order> entrys = new List<Order>();
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<Order[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return View(entrys[0]);
        }


        //public string DocumentPath
        //{
        //    get
        //    {
        //        var documentPath = System.IO.Path.Combine(
        //            env.ContentRootPath,
        //            DirectoryManagmentUtils.GetDocumentSampleFolderPath(HttpContext), DocumentName);
        //        return documentPath;
        //    }
        //}

        //public ActionResult Export()
        //{
        //    ViewData["DocumentPath"] = DocumentPath;
        //    return View();
        //}


        public IActionResult DownloadXlsx(SpreadsheetClientState spreadsheetState)
        {
            var spreadsheet = SpreadsheetRequestProcessor.GetSpreadsheetFromState(spreadsheetState);

            MemoryStream stream = new MemoryStream();
            spreadsheet.SaveCopy(stream, DocumentFormat.Xlsx);
            stream.Position = 0;
            return File(stream, XlsxContentType, "ProfitAndLoss.xlsx");
        }

    }

}
