#pragma checksum "D:\DeliveryApp\delivery_app\DeliveryApp\Views\UserOrder\Map.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "311b43b4a88fe6480bc262e108686aafa322844e"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_UserOrder_Map), @"mvc.1.0.view", @"/Views/UserOrder/Map.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\DeliveryApp\delivery_app\DeliveryApp\Views\_ViewImports.cshtml"
using DeliveryApp;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\DeliveryApp\delivery_app\DeliveryApp\Views\_ViewImports.cshtml"
using DeliveryApp.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "D:\DeliveryApp\delivery_app\DeliveryApp\Views\_ViewImports.cshtml"
using DevExtreme.AspNet.Mvc;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"311b43b4a88fe6480bc262e108686aafa322844e", @"/Views/UserOrder/Map.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"b0048b11abb10ab87f19fc78accaa6c5f2097b8f", @"/Views/_ViewImports.cshtml")]
    public class Views_UserOrder_Map : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/lib/jquery/dist/jquery.min.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "D:\DeliveryApp\delivery_app\DeliveryApp\Views\UserOrder\Map.cshtml"
  
    ViewBag.Title = "Map Page";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<h2>Bing Map integration in ASP.NET</h2>\r\n<input type=\"button\" id=\"btnShowLocations\" value=\"Show All Locations\" />\r\n");
            WriteLiteral(@"<input type=""button"" value=""Start Continuous Tracking"" onclick=""StartTracking()"" />
<input type=""button"" value=""Stop Continuous Tracking"" onclick=""StopTracking()"" />
<p id=""rData""></p>
<p id=""demo""></p>
<div id='printoutPanel'></div>
<div id='searchBoxContainer'><input type='text' id='searchBox' /></div>
<div id=""myMap"" style=""position:relative; width:100%; height:600px;""></div>
<script");
            BeginWriteAttribute("src", " src=\"", 608, "\"", 686, 2);
            WriteAttributeValue("", 614, "https://cdn.jsdelivr.net/npm/", 614, 29, true);
            WriteLiteral("@");
            WriteAttributeValue("", 645, "goongmaps/goong-js@1.0.9/dist/goong-js.js", 645, 41, true);
            EndWriteAttribute();
            WriteLiteral("></script>\r\n<link");
            BeginWriteAttribute("href", " href=\"", 704, "\"", 784, 2);
            WriteAttributeValue("", 711, "https://cdn.jsdelivr.net/npm/", 711, 29, true);
            WriteLiteral("@");
            WriteAttributeValue("", 742, "goongmaps/goong-js@1.0.9/dist/goong-js.css", 742, 42, true);
            EndWriteAttribute();
            WriteLiteral(" rel=\"stylesheet\" />\r\n<!-- Load the `goong-geocoder` plugin. -->\r\n<script");
            BeginWriteAttribute("src", " src=\"", 858, "\"", 952, 2);
            WriteAttributeValue("", 864, "https://cdn.jsdelivr.net/npm/", 864, 29, true);
            WriteLiteral("@");
            WriteAttributeValue("", 895, "goongmaps/goong-geocoder@1.1.1/dist/goong-geocoder.min.js", 895, 57, true);
            EndWriteAttribute();
            WriteLiteral("></script>\r\n<link");
            BeginWriteAttribute("href", " href=\"", 970, "\"", 1062, 2);
            WriteAttributeValue("", 977, "https://cdn.jsdelivr.net/npm/", 977, 29, true);
            WriteLiteral("@");
            WriteAttributeValue("", 1008, "goongmaps/goong-geocoder@1.1.1/dist/goong-geocoder.css", 1008, 54, true);
            EndWriteAttribute();
            WriteLiteral(@"
      rel=""stylesheet""
      type=""text/css"" />

<!-- Promise polyfill script is required -->
<!-- to use Goong Geocoder in IE 11. -->
<script src=""https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.min.js""></script>
<script src=""https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.auto.min.js""></script>

");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "311b43b4a88fe6480bc262e108686aafa322844e6309", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n<script type=\"text/javascript\">\r\n");
            WriteLiteral(@"
   


    var geojson = {
        'type': 'FeatureCollection',
        'features': [
            {
                'type': 'Feature',
                'properties': {
                    'message': 'Foo',
                    'iconSize': [60, 60]
                },
                'geometry': {
                    'type': 'Point',
                    'coordinates': [105.78903, 21.02524]
                }
            },
            {
                'type': 'Feature',
                'properties': {
                    'message': 'Bar',
                    'iconSize': [50, 50]
                },
                'geometry': {
                    'type': 'Point',
                    'coordinates': [105.79553, 21.03027]
                }
            },
            {
                'type': 'Feature',
                'properties': {
                    'message': 'Baz',
                    'iconSize': [40, 40]
                },
                'geometry': {
                    '");
            WriteLiteral(@"type': 'Point',
                    'coordinates': [105.79621, 21.00963]
                }
            }
        ]
    };

    goongjs.accessToken = '9IOlUggYUhs9DM8hgiw1Z4LhoGu8YRkLUc54xVxD';
    var map = new goongjs.Map({
        container: 'myMap',
        style: 'https://tiles.goong.io/assets/goong_map_web.json',
        center: [105.80028, 21.0188],
        zoom: 13
    });

    // Add the search control to the map.
    map.addControl(
        new GoongGeocoder({
            accessToken: 'rDCEpOhGmuPf78dMFbiLh7jNEzeHaJEAj2qVM9ns',
            goongjs: goongjs
        })
    );

    // add markers to map
    geojson.features.forEach(function (marker) {
        // create a DOM element for the marker
        var el = document.createElement('div');
        el.className = 'marker';
        el.style.backgroundImage =
            'url(https://placekitten.com/g/' +
            marker.properties.iconSize.join('/') +
            '/)';
        el.style.width = marker.properties.icon");
            WriteLiteral(@"Size[0] + 'px';
        el.style.height = marker.properties.iconSize[1] + 'px';

        el.addEventListener('click', function () {
            window.alert(marker.properties.message + ""-"" + marker.geometry.coordinates);
        });


        var url = ""/UserOrder/Map/"";
        var id = '");
#nullable restore
#line 114 "D:\DeliveryApp\delivery_app\DeliveryApp\Views\UserOrder\Map.cshtml"
             Write(ViewBag.OrderID);

#line default
#line hidden
#nullable disable
            WriteLiteral(@"';
        $.getJSON(url + id + '/GetLocations', null, function (data) {
            $.each(data, function (index, locationData) {
                var lat = locationData.Latitude;
                var lng = locationData.Longitude;
                // add marker to map
                new goongjs.Marker(el)
                    .setLngLat(new goongjs.LngLat(lng, lat))
                    .addTo(map);
            });
        });

    });




");
            WriteLiteral(@"
    //$('#btnShowLocations').click(function () {
    //    var posi
    //    $(""#rData"").html(""Latitude: "" + position.coords.latitude +
    //        ""<br>Longitude: "" + position.coords.longitude);
    //});

    $('#btnShowLocations').click(function () {
        getLocation();
    });

    var x = document.getElementById(""demo"");
    var lng = """";
    var lat = """";

    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else {
            x.innerHTML = ""Geolocation is not supported by this browser."";
        }
    }

    function showPosition(position) {
        lng = position.coords.longitude;
        lat = position.coords.latitude;
        window.alert(""Latitude: "" + lat +
            ""<br>Longitude: "" + lng);

    }

</script>
");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
